browser.storage.local.get().then(({ containers, nh_containers }) => {
    containers.forEach((container) => {
        const div = document.createElement("div")
        const checkbox = document.createElement("input")
        checkbox.type = "checkbox"
        checkbox.id = container.cookieStoreId
        if (nh_containers !== undefined && nh_containers.has(container.cookieStoreId)) {
            checkbox.checked = true
        }
        const label = document.createElement("label")
        label.htmlFor = container.cookieStoreId
        label.textContent = container.name

        checkbox.addEventListener("change", (event) => {
            browser.storage.local.get("nh_containers").then(({ nh_containers }) => {
                if (event.target.checked) {
                    if (nh_containers === undefined) {
                        nh_containers = new Set([event.target.id])
                    } else {
                        nh_containers.add(event.target.id)
                    }
                } else {
                    nh_containers.delete(event.target.id)
                }
                browser.storage.local.set({ nh_containers: nh_containers })
            })
        })

        div.appendChild(checkbox)
        div.appendChild(label)
        document.body.appendChild(div)
    })
})
