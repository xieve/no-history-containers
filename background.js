function cache_containers() {
    browser.contextualIdentities.query({}).then((containers) => {
        browser.storage.local.set({ containers })
    })
}

cache_containers()

browser.contextualIdentities.onUpdated.addListener(cache_containers)
browser.contextualIdentities.onRemoved.addListener(cache_containers)
browser.contextualIdentities.onCreated.addListener(cache_containers)

browser.history.onVisited.addListener(async (history_item) => {
    // i hope to mitigate a race condition by copying this value at the earliest possible time
    const timestamp = history_item.lastVisitTime
    console.log(`[${timestamp}] History: ${history_item.url}`)
    const { deletion_candidates } = await browser.storage.local.get("deletion_candidates")
    const origin = new URL(history_item.url).origin

    if (deletion_candidates !== undefined && deletion_candidates[origin] !== undefined) {
        if (timestamp > deletion_candidates[origin] + 2000) {
            delete deletion_candidates[origin]
        } else {
            console.log(`Deleting visit of ${history_item.url} at ${timestamp}`)
            await browser.history.deleteRange({
                startTime: timestamp,
                endTime: timestamp,
            })
        }
    }
})

async function maybe_delete_visit(details) {
    let { nh_containers, deletion_candidates } = await browser.storage.local.get([
        "nh_containers",
        "deletion_candidates",
    ])
    if (deletion_candidates === undefined) {
        deletion_candidates = {}
    }
    if (details.cookieStoreId === undefined) {
        details.cookieStoreId = (await browser.tabs.get(details.tabId)).cookieStoreId
    }
    if (nh_containers !== undefined && nh_containers.has(details.cookieStoreId)) {
        // this is kind of a race condition but i don't think it should be more of a problem than all the rest
        deletion_candidates[new URL(details.url).origin] = details.timeStamp
    }
    await browser.storage.local.set({ deletion_candidates })
}

browser.webNavigation.onCommitted.addListener(async (details) => {
    console.log(`[${details.timeStamp}] committed: ${details.url}`)
    await maybe_delete_visit(details)
})

browser.webNavigation.onHistoryStateUpdated.addListener(async (details) => {
    console.log(`[${details.timeStamp}] history state updated: ${details.url}`)
    await maybe_delete_visit(details)
})

browser.webRequest.onHeadersReceived.addListener(
    async (details) => {
        console.log(`[${details.timeStamp}] headers received: ${details.url}`)
        await maybe_delete_visit(details)
    },
    { urls: ["<all_urls>"], types: ["main_frame"] }
)
browser.webRequest.onBeforeRequest.addListener(
    async (details) => {
        console.log(`[${details.timeStamp}] before request: ${details.url}`)
        await maybe_delete_visit(details)
    },
    { urls: ["<all_urls>"], types: ["main_frame"] }
)
